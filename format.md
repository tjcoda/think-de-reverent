## THINK Reference

_Best guess at the format_

## Glossary

Where possible original terminology has been used to keep consistent.

- Volume
- Issue
- Page - An article which a hyperlink can lead to (PgDr/PageDirectory Resource Type)

## WIP Notes

- Develop 1991 - SYSTEM 7.0 Q & AMACINTOSH DEVELOPER TECHNICAL SUPPORT

There are lines in the same paragraph receiving different styles (second one font is crazy big) which indicates that parsing the style from the string is not working correctly.


If a db is moved to its own directory away from the index db then none of the buttons which normally go to the index work. So the index db individually stores all the indices and files must either cross reference or follow a filename convention.

Packbits compression (an apple run-length encoding) could be used. The hex 80 (128) always appears to be followed by 00 or 01 which would indicate runlength encodings of 1 or 2. After trying packbits at various offsets into the page it appears to not be the compression used. Many cases of string which appear complete being correupted by the 'decoding'.

80 - No operation, treat next byte as header byte
0 - 7F - 1 + n literal bytes of data
FF to 81 - one byte of data repeated 1 - N times



PgDr page offsets get us to the start of pages, which are marked with `0x4452000000` followed by a string.

Its been suggested that the strings are probably using style runs, but they don't immediately appear to match those structs.

## Resource Fork

### BInf : Button Information

The viewer has four buttons on the left-hand side which change based on the database file being used. This contains the text for them.

### FInf

Numbers are too low to be offsets or size of articles, however the size of this resource indicates it may be a U16 for each article

### Prfs/132 : db Version - 0x07F9

### GInf : Guide Info

`00` <- SAME
`7B 00 00 CA 06 00 16` <- Diff
`00 00 00 04` <- same

### CpTk : Cmprs Tokens

Compression Tokens are text strings which appear often enough to be stored in this resource and referenced. Strings in the data fork are in MacRoman encoding, they have a 2 byte length followed by the string which can contain zero bytes followed by the index into CpTk to reference the token to be inserted as part of the string. There are also some special codes which modify the token as it is inserted, e.g. 0xFC = Capitalise token 0xFB = Capitalise token and add a space

### lmXr : Empty

### PgDr : Page Directory

List<U32> - Byte Offsets of Pages in the Data Fork
Appears to match file offsets for the article starting markers

### xdef

A common type, based on WDEF

An alphabetically sorted (possibly coincidentally) list of article link names (the names of the hyperlinks rather than the test preceding them)

Possible an index?

### WTxt

**Not Sure what this is for**

Each resource ID appears to be a list of words starting with a certain letter, e.g. 139 = a, 140 = c, etc 164 = z
but missing that letter.

Presumably the resource IDs starting at 128 begin with a numerical digit.

When indexing into this, the first byte is the length of the string (string misses off the first character)

### WDat

Pairs of 32bit Uints. The first in the pair is the byte offset in WTxt of a string. The second in the pair is a sorted number which appears to increase to around the size of the data fork, indicating it is the location to insert the string.

Resource IDs and rough sizes match the WTxT
Corresponding byte offsets for the WTxt entries?

## Data Fork

Articles appear in order, offsets are indexed using PgDr (Page Directory) in the resource fork.

### File Marker

`0x4E474D43` (NGMC)

### File Sub Marker

`0x005D`

### Page Marker

`0x445200000000`

### Page Title

- UInt16 - String Length
- MacRoman String


### String

- UInt16 - Byte Length
- MacRoman Characters

Mode Switches (within string)
- C1 - Bold
- C2 - Italic
- C3 - Bold + Italic
- FB - CpTk - insert capitalised token, then a space character 
- FC - CpTk - insert capitalised token
- FD - CpTk - insert token and insert space after
- FE - CpTk - insert space and then insert token

I believe that is the byte length is odd then you should read one additional padding zero byte at the end

