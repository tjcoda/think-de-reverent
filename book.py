import kaitaistruct
import os
import html
import page
import assets
from resources import compressiontokens, pagedirectory, font, images, xdef

class Book:

    def __init__(self, bookFile):
        self.data = bookFile.data
        self.processResourceFork(bookFile.rsrc)

    def processResourceFork(self, resourceBytes):
        pagedir = pagedirectory.PageDirectory()
        self.pageOffsets = pagedir.getFromByteArray(resourceBytes)
        self.compressionTokens = compressiontokens.CompressionTokens().getFromByteArray(resourceBytes)
        self.fontMapper = font.FontMapper(resourceBytes)
        self.imageProvider = images.ImageProvider(resourceBytes)
        self.pages = xdef.XDef().get(resourceBytes)

    def getPageTitles(self):
        return self.pages

    def renderPage(self, pageIndex, pathPrefix):
        pageData = self.data[self.pageOffsets[pageIndex][0]: self.pageOffsets[pageIndex][1]]
        p = page.Page(kaitaistruct.KaitaiStream(kaitaistruct.BytesIO(pageData)))
        outputPage = html.HTML(self.fontMapper, self.imageProvider, depth=1)
        lastStyle = None
        for chunkIndex in range(0, len(p.chunk)):
            chunk = p.chunk[chunkIndex]

            processedAssets = assets.readAssetsFromPascalString(chunk.content.text, lastStyle, self.compressionTokens)

            for asset in processedAssets:
                outputPage.addAsset(chunk.xcoord, chunk.ycoord, asset)

        return outputPage.generateHTML()

    def renderIndex(self, pathPrefix):
        destinationFolder = 'output'
        outputPage = html.HTML(self.fontMapper, self.imageProvider)

        listOfLinks = []

        for pageIndex in range(0, len(self.pages)):
            title = self.pages[pageIndex]
            link = {'INDEX': pageIndex, 'TEXT': title}
            listOfLinks.append(link)

        outputPage.addListOfLinks(pathPrefix, listOfLinks)
        return outputPage.generateHTML()
