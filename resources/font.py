import rez

import sys
import os
import getopt

from io import BytesIO

# Character Format Information
#
# Best Guess
# 2 bytes - ? can be 0x0002, 0x0003, 0xFFFF
# 2 bytes - ? tends to be 0x0009 or 0x000A or 0x000C, 9, 10, 12 which are common font sizes
# 2 bytes - 0,1,2,3,4
# 2 bytes - ? same as font size
# 2 bytes - ? tends to be 0x0003


class FontMapper:

    def __init__(self, resourceBytes):
        self.__load(resourceBytes)

    def get(self, id):
        return self.__fontInfos[id]

    def fontIDToFamily(self, id):

        default = 'Arial'
        codeFont = 'Courier'

        fontIDToNameMap = {
            0: default,  # System Font
            1: default,  # Application Font
            2: default,  # New York
            3: default,  # Geneva
            4: codeFont,  # Monaco
            5: default,  # Venice
            6: default,  # London
            7: default,  # Athens
            8: default,  # San Francisco
            9: default,  # Toronto
            10: default,
            11: default,  # Cairo
            12: default,  # Los Angeles
            20: 'Times',  # Times
            21: default,  # Helvetica
            22: 'Courier',  # Courier
            23: default,  # Symbol
            24: default   # Mobile
        }

        if not hasattr(fontIDToNameMap, f'{id}'):
            return default

        return fontIDToNameMap[id]

    def count(self):
        return len(self.__fontInfos)

    def __load(self, resourceBytes):
        self.__fontInfos = []

        data = rez.getFromByteArray(resourceBytes, 'FInf', 128)

        f = BytesIO(data)
        while f.tell() < len(data):
            FONTID = int.from_bytes(f.read(2), 'big')
            SIZE = int.from_bytes(f.read(2), 'big')
            C = int.from_bytes(f.read(2), 'big')
            D = int.from_bytes(f.read(2), 'big')
            E = int.from_bytes(f.read(2), 'big')

            style = {'SIZE': SIZE}
            style['FONT'] = self.fontIDToFamily(FONTID)

            if C & 1 != 0:
                style['BOLD'] = True

            if C & 2 != 0:
                style['ITALIC'] = True

            if C & 4 != 0:
                style['UNDERLINED'] = True

            if C & 8 != 0:
                style['OUTLINED'] = True

            if C & 16 != 0:
                style['SHADOWED'] = True

            if C & 32 != 0:
                style['CONDENSED'] = True

            if C & 32 != 0:
                style['EXTENDED'] = True

            self.__fontInfos.append(style)
