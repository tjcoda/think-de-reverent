import rez


class CompressionTokens:

    def getFromByteArray(self, resourceBytes):
        tokens = []
        data = rez.getFromByteArray(resourceBytes, 'CpTk', 128)

        if data is None:
            return tokens

        filelength = len(data)

        offset = 2
        while (offset < filelength):
            bytearr = bytearray()
            byteLen = data[offset]

            for i in range(0, byteLen):
                bytearr += data[offset + i + 1].to_bytes(1, 'big')

            tokens.append(bytearr.decode('mac_roman'))
            offset += 32

        return tokens
