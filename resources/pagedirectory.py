import rez

import sys
import os
import getopt

import struct


class PageDirectory:

    def get(self, fileName):
        data = rez.getIndividualData(fileName, 'PgDr', 128)
        pageDirectory = struct.unpack(
            '>' + 'I'*(len(data) >> 2), data)
        return pageDirectory

    def getFromByteArray(self, resourceBytes):
        data = rez.getFromByteArray(resourceBytes, 'PgDr', 128)
        pageDirectory = struct.unpack(
            '>' + 'I'*(len(data) >> 2), data)

        return list(zip(pageDirectory[:-1], pageDirectory[1:]))
