import rez
import os
import shutil
import command
from PIL import Image
from io import BytesIO

import base64

class ImageProvider:

    def __init__(self, resourceBytes):
        self.resourceBytes = resourceBytes

    def getImageData(self, index):
        data = rez.getFromByteArray(self.resourceBytes, 'PICT', index)
        if data is None:
            return None

        imageData = command.Command('deark -tostdout -noinfo -fromstdin').run(data)
        bytes_obj = bytes(imageData)

        # Encode the bytes object to Base64
        encoded_bytes = base64.b64encode(bytes_obj)

        # Convert the encoded bytes to a string
        encoded_str = encoded_bytes.decode('utf-8')

        try:
            img_file = BytesIO(imageData)
            # Open the image
            img = Image.open(img_file)
            # Get the dimensions
            width, height = img.size
            img.close()

            return { 'base64': encoded_str, 'width' : width, 'height': height}

        except:
            print(f'Failed to convert image')
            return None