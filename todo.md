# Things to do

## Done

+ Generate an index page
+ Scaling images (not all PICT images display at 100%)

# Remaining

- Offsetting text horizontally - take the previous string/s in the asset, add up their width (including nbsp) and add to x coord? Seems to nearly work, but Volume info at top of page doesn't line up nicely
- Some images don't export from deark!
- Inside Macintosh - much harder to parse
- Figure out the two (or more) Page formats which are different to normal page, possibly cross reference?
- Links between pages

# Nice to have

- BInf - Button Text
- Dark mode