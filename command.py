import subprocess


class Command:

    def __init__(self, command):
        self.command = command

    def run(self, inputBytes):
        proc = subprocess.Popen(
            [self.command], stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate(input=inputBytes)
        return out
