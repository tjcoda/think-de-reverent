import sys
import os
import getopt
import shutil
import rsrcfork
import io
import macresources
import platform

# Only read actual resource fork on mac OS
def getIndividualData(fileName, rType, index):
    v, _, _ = platform.mac_ver()
    if len(v) > 0:
        return getIndividualData_macos(fileName, rType, index)
    else:
        return getIndividual_multiplatform(fileName, rType, index)

def getIndividualData_macos(fileName, rType, index):
    rf = rsrcfork.open(fileName)
    resType = bytes(rType, 'mac_roman')
    resources = rf[resType]

    if not index in resources:
        return None

    resource = resources[index]
    return resource.data


def getIndividual_multiplatform(fileName, rType, index):
    resType = bytes(rType, 'mac_roman')
    with open('{}.rdump'.format(fileName), 'rb') as f:
        raw = f.read()
        cached_resources = list(macresources.parse_rez_code(raw))
        resources = [foundres for foundres in cached_resources if foundres.type ==
                     resType and foundres.id == index]
        if len(resources) == 0:
            return None

        return resources[0]

def getFromByteArray(resourceBytes, rType, index):
    wrappedBytes = io.BytesIO(resourceBytes)
    rf = rsrcfork.ResourceFile(wrappedBytes)
    resType = bytes(rType, 'mac_roman')
    resources = rf[resType]

    if not index in resources:
        return None

    resource = resources[index]
    return resource.data

def get(fileName, rType):
    resType = bytes(rType, 'mac_roman')
    with open('{}.rdump'.format(fileName), 'rb') as f:
        raw = f.read()
        cached_resources = list(macresources.parse_rez_code(raw))
        resources = [
            foundres for foundres in cached_resources if foundres.type == resType]
        return resources


def dumpResource(resourceFilename, resourceType):
    os.makedirs(resourceType, exist_ok=True)
    os.system(
        'rfx cp "{}".rdump//{} {}'.format(resourceFilename, resourceType, resourceType))


def clearResource(resourceType):
    if len(resourceType) != 4:
        return
    try:
        os.system('rm -r {}'.format(resourceType))
        shutil.rmtree(resourceType)
    except:
        print()
