from machfs import Volume, Folder, File
import macresources
import book

# TODO - host a web server and display HTML output

iso_file = '/home/henryj/Downloads/MacTech-vol-1-12.toast'

with open(iso_file, 'rb') as f:
    flat = f.read()
    v = Volume()
    v.read(flat)
    
    # Navigate to folder
    archives = v['Reference'][' the MacTech Archives']
    #for item in archives:
    #    print(item)

    # Access File
    vol1 = archives['MacTech Vol 01-1984/5']

    b = book.Book(vol1)
    b.renderIndex()
    b.renderPage(10)