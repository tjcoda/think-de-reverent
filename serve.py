import click
from flask import Flask, jsonify, request
from machfs import Volume, Folder, File
import os
import book

volume = None

@click.group()
def main():
    """Do nothing"""

@click.command()
@click.option('--iso', default='')
def web(iso: str):
    app = Flask(__name__)

    app.config["iso"] = iso

    @app.route('/')
    @app.route('/<path:varargs>')
    def iso_path(varargs=None):
        iso_file = app.config.get('iso')
        with open(iso_file, 'rb') as f:
            flat = f.read()
            volume = Volume()
            volume.read(flat)
            if varargs:
                pathSegments = varargs.split('/')
                for index, segment in enumerate(pathSegments):
                    if len(segment) > 0 and not segment.isspace():
                        volume = volume[segment]
                        if hasattr(volume, 'data'):
                            # End of path is a file, show the index
                            if index + 1 == len(pathSegments):
                                b = book.Book(volume)
                                return b.renderIndex(request.path)
                            else:
                                # Path extends into file, show the page
                                b = book.Book(volume)
                                return b.renderPage(int(pathSegments[-1]), request.path)
                            break



            fileListing = list(map((lambda x: f'<p><a href="{os.path.join(request.path, x)}">"{x}"</a></p>'), filter((lambda s : not s.isspace()), volume.keys())))
            return f'<p>Contents of Dir {request.path}</p><p></p>{' '.join(fileListing)}'

    app.run()


if __name__ == '__main__':
    web()

