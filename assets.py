from io import BytesIO

def readAssetsFromPascalString(pascalString, lastStyle, compressionTokens):
    # print('Reading complex string at offset 0x{:02X}'.format(fin.tell()))

    hadImage = False

    output = []

    byteLen = len(pascalString)

    if byteLen == 0:
        return ''

    buffer = bytearray(pascalString, 'mac_roman')

    f = BytesIO(buffer)

    xOffsetInCharacters = 0

    bytearr = bytearray()

    bytesRead = 0
    currentStyle = 0

    if lastStyle is not None:
        currentStyle = lastStyle

    nextByteIsLiteral = False
    nextTokenModifier = None

    while (bytesRead < byteLen):
        b = f.read(1)
        bytesRead += 1

        if b >= b'\xf0' and b <= b'\xfe':
            nextTokenModifier = b
        elif b == b'\x00':
            # If we land on a null don't assume there is more to read
            if bytesRead == byteLen:
                break

            tokenId = int.from_bytes(f.read(1), 'big')
            bytesRead += 1

            # print('{} {}'.format(bytearr, tokenId))

            if nextTokenModifier == b'\xf0':  # Image
                # Store whatever we have so far as a string
                if len(bytearr) > 0:
                    output.append({'TEXT': bytearr.decode('mac_roman'),
                                   'STYLE': currentStyle})
                    bytearr = bytearray()

                width = int.from_bytes(f.read(2), 'big')
                bytesRead += 2
                output.append({'IMAGE': tokenId, 'WIDTH': width,
                              'XOFFSETCHARS': xOffsetInCharacters})
                hadImage = True
            elif tokenId < len(compressionTokens):
                tokenString = compressionTokens[tokenId]
                if nextTokenModifier == b'\xfb':
                    tokenString = tokenString.capitalize() + ' '
                elif nextTokenModifier == b'\xfc':
                    tokenString = tokenString.capitalize()
                elif nextTokenModifier == b'\xfd':
                    tokenString = tokenString + ' '
                elif nextTokenModifier == b'\xfe':
                    tokenString = ' ' + tokenString

                token = bytes(tokenString, 'mac_roman')
                bytearr += token
            else:
                print('Couldnt interpret string value {}'.format(tokenId))
            # Reset the token modifier
            nextTokenModifier = None
        elif b >= b'\xc1' and b <= b'\xcf':
            if len(bytearr) > 0:
                output.append({'TEXT': bytearr.decode('mac_roman'),
                               'STYLE': currentStyle})
            bytearr = bytearray()
            currentStyle = int.from_bytes(b, 'big') - 193
        elif b == b'\xff':
            nextByteIsLiteral = True
        elif b >= b'\x80' and b <= b'\xd8' and not nextByteIsLiteral:
            spaceOffsetPixels = int.from_bytes(b, 'big')
            spaces = int(spaceOffsetPixels / 10)
            xOffsetInCharacters += spaces
            for spaceIndex in range(spaces):
                bytearr += '&nbsp;'.encode('mac_roman')
        else:
            bytearr += b
            xOffsetInCharacters += 1
            nextByteIsLiteral = False

    if len(bytearr) > 0:
        output.append({'TEXT': bytearr.decode('mac_roman'),
                       'STYLE': currentStyle})

    if hadImage:
        with open("images.txt", "a") as file:
            file.write('Image found in {}\n'.format(buffer.hex()))
        # print('{} : {}'.format(bytearr, bytearr.decode('mac_roman')))
    return output
