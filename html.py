import domonic
import os


class HTML:

    def __init__(self, fontMapper, imageProvider, depth=0):
        self.fontMapper = fontMapper
        self.imageProvider = imageProvider
        self.site = domonic.html()
        self.lastYCoord = 0

        head = self.site.createElement('head')
        head.append(domonic.meta(_content='text/html; charset=utf-8'))
        self.site.append(head)

        style = self.site.createElement('style')
        head.append(style)

        styleText = self.site.createTextNode(self.generateCSS())
        style.append(styleText)

        self.container = self.site.createElement('div')
        self.container.style.backgroundColor = 'white'
        self.container.style.width = '450px'
        self.container.style.margin = 'auto'
        self.container.style.position = 'relative'

        self.site.append(self.container)
        self.site.style.backgroundColor = '#777777'

    def generateCSS(self):
        s = ''
        for id in range(0, self.fontMapper.count()):
            style = self.fontMapper.get(id)
            s += '.style{} {{\n'.format(id)
            s += '\tfont-family: {};\n'.format(style['FONT'])
            s += '\tfont-size: {};\n'.format(style['SIZE'])

            if 'BOLD' in style:
                s += '\tfont-weight: bold;\n'

            if 'ITALIC' in style:
                s += '\tfont-style: italic;\n'

            s += '}\n'
        return s

    def getTextElementFromAsset(self, asset):
        global site
        global fontMapper
        text = self.site.createTextNode(asset['TEXT'])
        styleIndex = asset['STYLE']

        span = domonic.span(_class="style{}".format(styleIndex + 1))
        span.append(text)

        return span

    def getImageElementFromAsset(self, lastYCoord, xcoord, ycoord, asset):
        imageIndex = asset['IMAGE']
        width = asset['WIDTH']
        xOffsetInCharacters = asset['XOFFSETCHARS']

        # TODO - Reimplement
        im = self.imageProvider.getImageData(imageIndex)

        if im is None:
            div = self.site.createElement('div')
            div.append(self.site.createTextNode('Missing image Resource!'))
            return div

        imageNode = domonic.img(_src=f'data:image/png;base64,{im['base64']}', _width='{}'.format(width))

        y = ycoord - im['height']

        # Nasty Hacks
        if y < 0:
            y = 0
        if y < self.lastYCoord + 11:
            y = self.lastYCoord + 11

        div = self.site.createElement('div')
        div.style.position = 'absolute'
        div.style.left = '{}px'.format(xcoord + (9 * xOffsetInCharacters))
        div.style.top = '{}px'.format(y)
        div.append(imageNode)

        return div

    def addListOfLinks(self, prefix, listOfLinks):
        ul = domonic.ul()
        ul.style.fontFamily = "'Arial'"

        for index in range(0, len(listOfLinks)):
            link = listOfLinks[index]

            li = domonic.li()
            path = '{}/{}'.format(prefix, link['INDEX'])
            container = domonic.a(_href=path)
            text = self.site.createTextNode(link['TEXT'])
            container.append(text)
            li.append(container)
            ul.append(li)

        self.site.append(ul)

    def addAsset(self, x, y, asset):
        if 'TEXT' in asset:
            style = asset['STYLE']

            paragraph = self.site.createElement('div')
            paragraph.style.position = 'absolute'
            paragraph.style.padding = '0'
            paragraph.style.margin = '0'
            paragraph.style.left = '{}px'.format(x)
            paragraph.style.top = '{}px'.format(y)
            paragraph.append(self.getTextElementFromAsset(asset))
            self.container.append(paragraph)

            lastStyle = style
        if 'IMAGE' in asset:
            self.container.append(self.getImageElementFromAsset(
                self.lastYCoord, x, y, asset))

        self.lastYCoord = y

    def generateHTML(self):
        if self.container:
            self.container.style.height = '{}px'.format(self.lastYCoord)
        return self.site.toString()
