# Intro

Viewer for archaic THINK Reference databases. Reads an HFS volume containing THINK Reference database and serves a webpage as a viewer.

# Disclaimer

I have no affiliation with THINK. This program is purely made through guesswork, is not guaranteed to work correctly and should not be used. Thanks.

# Getting the content

Find a disk image in the HFS format which contains THINK references, e.g. MacTech CD-ROMs on macintosh garden.

# Prerequisites

- Python 3
- deark - (utility to convert PICT resources to PNG, so good it doesn't even need headers) - https://github.com/jsummers/deark
- Imagemagick - to combine PNG when they come out as strips instead of whole images

Run ```setup.sh``` to install dependencies and create virtual python environment.

# Usage

```./serve.sh <path to iso>```

# Contributing

Please help!

[List of things to do](todo.md)
